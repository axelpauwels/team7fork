package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.Boek;
import com.realdolmen.thomasmore.domain.Item;
import com.realdolmen.thomasmore.domain.User;
import com.realdolmen.thomasmore.domain.Winkelmand;
import com.realdolmen.thomasmore.service.ItemService;
import com.realdolmen.thomasmore.service.WinkelmandService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.text.SimpleDateFormat;
import java.util.*;

@ManagedBean
@SessionScoped
public class AdminBestellingenBeherenController {

    @ManagedProperty("#{winkelmandService}")
    private WinkelmandService winkelmandService;
    @ManagedProperty("#{itemService}")
    private ItemService itemService;

    private GregorianCalendar datum;
    private List<GregorianCalendar> datums;

    private User user;

    private Item updateItem;

    private Winkelmand huidigeWinkelmand;
    private List<Winkelmand> winkelmanden;

    private long winkelmandId;
    private long boekId;
    private Boek boek;
    private int aantal;
    private String zoekFunctie = "";


    public void onUserChange() {
        if(user !=null && !user.equals("")){
            winkelmanden = winkelmandService.getAllWinkelmandenByUser(user);
        }
        zoekFunctie = "User";
    }
    public void onDateChange() {
        if(datum !=null && !datum.equals("")){
            GregorianCalendar startDatum = new GregorianCalendar(datum.get(Calendar.YEAR),datum.get(Calendar.MONTH),datum.get(Calendar.DAY_OF_MONTH),0,0,0);
            GregorianCalendar eindDatum = new GregorianCalendar(datum.get(Calendar.YEAR),datum.get(Calendar.MONTH),datum.get(Calendar.DAY_OF_MONTH),24,59,59);
            winkelmanden = winkelmandService.getAllWinkelmandenByDatum(startDatum,eindDatum);
        }
        zoekFunctie = "Datum";
    }
    public void onWinkelmandNrChange() {
        if(winkelmandId !=0 ){
            winkelmanden = new ArrayList<>();
            winkelmanden.add(winkelmandService.getWinkelmandById(winkelmandId));
        }else{
            winkelmanden.clear();
            winkelmanden.add(winkelmandService.getWinkelmandById(winkelmandId));
        }
        zoekFunctie = "BestellingNr";
    }
    public void wijzigItem(Item item) {
        if (updateItem == null) {
            updateItem = new Item();
        }
        updateItem.setId(item.getId());
        updateItem.setBoek(item.getBoek());
        updateItem.setAantal(item.getAantal());
        updateItem.setWinkelmand(item.getWinkelmand());
        winkelmandId = updateItem.getWinkelmand().getId();
        boek = updateItem.getBoek();
        aantal = updateItem.getAantal();
    }
    public void deleteItem(Item deleteItem) {
        System.out.println("*****deleteItem Controller");
        System.out.println(deleteItem);
        itemService.deleteItem(deleteItem.getId());
        getWinkelmandenOpZoekfunctie();
    }

    public void getWinkelmandenOpZoekfunctie() {
        if(zoekFunctie != ""){
            switch (zoekFunctie){
                case "User":
                    winkelmanden = winkelmandService.getAllWinkelmandenByUser(user);
                    break;
                case "Datum":
                    GregorianCalendar startDatum = new GregorianCalendar(datum.get(Calendar.YEAR),datum.get(Calendar.MONTH),datum.get(Calendar.DAY_OF_MONTH),0,0,0);
                    GregorianCalendar eindDatum = new GregorianCalendar(datum.get(Calendar.YEAR),datum.get(Calendar.MONTH),datum.get(Calendar.DAY_OF_MONTH),24,59,59);
                    winkelmanden = winkelmandService.getAllWinkelmandenByDatum(startDatum,eindDatum);
                    break;
                case "BestellingNr":
                    winkelmanden.clear();
                    winkelmanden.add(winkelmandService.getWinkelmandById(winkelmandId));
                    break;
            }
            user=null;
            datum=null;
            winkelmandId=0;
        }
    }
    public void updateItemOpslaan()  {
        updateItem.setBoek(boek);
        updateItem.setAantal(aantal);
        itemService.updateItem(updateItem);
        getWinkelmandenOpZoekfunctie();
    }

    public void updateItemAnnuleren() {}

    public void onChangeItemAantal() {}

    public String formatDate(Date date, String pattern) {
        return (new SimpleDateFormat(pattern)).format(date);
    }

    public List<GregorianCalendar> getDatumsVanBestellingen() {
        return winkelmandService.getDatumsVanBestellingen();
    }

    public WinkelmandService getWinkelmandService() {
        return winkelmandService;
    }

    public void setWinkelmandService(WinkelmandService winkelmandService) {
        this.winkelmandService = winkelmandService;
    }

    public ItemService getItemService() {
        return itemService;
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public GregorianCalendar getDatum() {
        return datum;
    }

    public void setDatum(GregorianCalendar datum) {
        this.datum  = datum;
    }

    public List<GregorianCalendar> getDatums() {
        return datums;
    }

    public void setDatums(List<GregorianCalendar> datums) {
        this.datums = datums;
    }

    public Item getUpdateItem() {return updateItem;}

    public void setUpdateItem(Item updateItem) {this.updateItem = updateItem;}

    public List<Winkelmand> getWinkelmanden() {
        return winkelmanden;
    }

    public void setWinkelmanden(List<Winkelmand> winkelmanden) {
        this.winkelmanden = winkelmanden;
    }

    public long getWinkelmandId() {return winkelmandId;}

    public void setWinkelmandId(long winkelmandId) {this.winkelmandId = winkelmandId;}

    public long getBoekId() {return boekId;}

    public void setBoekId(long boekId) {this.boekId = boekId; }

    public Boek getBoek() {return boek;}

    public void setBoek(Boek boek) {this.boek = boek;}

    public int getAantal() {return aantal;}

    public void setAantal(int aantal) {this.aantal = aantal;}

    public String getZoekFunctie() {return zoekFunctie;}

    public void setZoekFunctie(String zoekFunctie) {this.zoekFunctie = zoekFunctie;}


}
