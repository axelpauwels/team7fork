package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.Employee;
import com.realdolmen.thomasmore.service.EmployeeService;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.List;

/**
 * Created by JUZAU33 on 28/09/2017.
 */
@ManagedBean
@ViewScoped
public class EmployeeController {

    //Autowired kunnen we niet gebruiken in JSF, daarom gebruiken we hier dit om een spring bean te injecteren.
    @ManagedProperty("#{employeeService}")
    private EmployeeService employeeService;

    private String newEmployeeFirstName;
    private String newEmployeeLastName;

    public List<Employee> getEmployees() {
        return employeeService.findAllEmployees();
    }

    public void createEmployee() {
        employeeService.createEmployee(newEmployeeFirstName, newEmployeeLastName);
        addMessage("Employee toegevoegd!");
        clearForm();
    }

    private void clearForm() {
        newEmployeeFirstName = null;
        newEmployeeLastName = null;
    }

    private void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public String getNewEmployeeFirstName() {
        return newEmployeeFirstName;
    }

    public String getNewEmployeeLastName() {
        return newEmployeeLastName;
    }

    public void setNewEmployeeFirstName(String newEmployeeFirstName) {
        this.newEmployeeFirstName = newEmployeeFirstName;
    }

    public void setNewEmployeeLastName(String newEmployeeLastName) {
        this.newEmployeeLastName = newEmployeeLastName;
    }

    /**
     * Deze setter MOET aanwezig zijn, anders kan spring deze service niet injecteren.
     */
    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }
}
