package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.service.*;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class HomeController {
    //Autowired kunnen we niet gebruiken in JSF, daarom gebruiken we hier dit om een spring bean te injecteren.
    @ManagedProperty("#{genreService}")
    private GenreService genreService;
    @ManagedProperty("#{auteurService}")
    private AuteurService auteurService;
    @ManagedProperty("#{uitgeverijService}")
    private UitgeverijService uitgeverijService;
    @ManagedProperty("#{boekService}")
    private BoekService boekService;
    @ManagedProperty("#{userService}")
    private UserService userService;
    @ManagedProperty("#{userRolService}")
    private UserRolService userRolService;
    @ManagedProperty("#{itemService}")
    private ItemService itemService;
    @ManagedProperty("#{winkelmandService}")
    private WinkelmandService winkelmandService;

    /** CONSTRUCTORS */
    public HomeController() {
    }

    /** FUNCTIONS */
    public String testPage(){
        return "test/test.xhtml?faces-redirect=true";
    }

    public void createAllDummyData(){
        genreService.createDummyData();
        auteurService.createDummyData();
        uitgeverijService.createDummyData();
        boekService.createDummyData();
        userRolService.createDummyData();
        userService.createDummyData();
        itemService.createDummyData();
        winkelmandService.generateTestData();
    }

    public String getHelloWorld() {
        return "Hello, world!";
    }

    /** GETTERS & SETTERS */

    /**Deze setter MOET aanwezig zijn, anders kan spring deze service niet injecteren.*/
    public GenreService getGenreService() {
        return genreService;
    }

    public void setGenreService(GenreService genreService) {
        this.genreService = genreService;
    }

    public AuteurService getAuteurService() {
        return auteurService;
    }

    public void setAuteurService(AuteurService auteurService) {
        this.auteurService = auteurService;
    }

    public UitgeverijService getUitgeverijService() {
        return uitgeverijService;
    }

    public void setUitgeverijService(UitgeverijService uitgeverijService) {
        this.uitgeverijService = uitgeverijService;
    }

    public BoekService getBoekService() {
        return boekService;
    }

    public void setBoekService(BoekService boekService) {
        this.boekService = boekService;
    }
<<<<<<< HEAD

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public UserRolService getUserRolService() {
        return userRolService;
    }

    public void setUserRolService(UserRolService userRolService) {
        this.userRolService = userRolService;
    }

    public ItemService getItemService() {
        return itemService;
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    public WinkelmandService getWinkelmandService() {
        return winkelmandService;
    }

=======
    public void setUserService(UserService userService) { this.userService = userService; }
    public void setUserRolService(UserRolService userRolService) { this.userRolService = userRolService; }
    public void setItemService(ItemService itemService) { this.itemService = itemService; }
>>>>>>> axel
    public void setWinkelmandService(WinkelmandService winkelmandService) {
        this.winkelmandService = winkelmandService;
    }
}
