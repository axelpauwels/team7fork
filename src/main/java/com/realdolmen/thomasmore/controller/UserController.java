package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.authentication.UserSession;
import com.realdolmen.thomasmore.domain.User;
import com.realdolmen.thomasmore.domain.UserRol;
import com.realdolmen.thomasmore.domain.Winkelmand;
import com.realdolmen.thomasmore.service.UserRolService;
import com.realdolmen.thomasmore.service.UserService;
import com.realdolmen.thomasmore.service.WinkelmandService;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import java.util.Comparator;
import java.util.List;

import static org.primefaces.component.menubutton.MenuButton.PropertyKeys.value;

@ManagedBean
@SessionScoped
public class UserController {

    @ManagedProperty("#{userService}")
    private UserService userService;

    @ManagedProperty("#{userRolService}")
    private UserRolService userRolService;

    @ManagedProperty("#{userSession}")
    private UserSession userSession;

    @ManagedProperty("#{winkelmandService}")
    private WinkelmandService winkelmandService;

    private UserRol userRol; //voor te weten wat de rol is
    private User nieuweUser;
    private String nieuweVoornaam;
    private String nieuweAchternaam;
    private String nieuweStraatEnHuisnummer;
    private int nieuwePostcode;
    private String nieuweGemeente;
    private String nieuweEmail;
    private String nieuwPassword;
    private String url;
    private Winkelmand winkelmand;
    private List<Winkelmand> winkelmanden;
    public List<Winkelmand> getWinkelmanden() {
        if (winkelmanden == null) {
            winkelmanden = winkelmandService.getAllWinkelmanden();
        }
        return winkelmanden;
    }

    private List<User> users;
    public List<User> getUsers() {
        if (users == null) {
            users = userService.findAllUsers();
        }
        users.stream().sorted(Comparator.comparing(User::getVoornaam));
        return users;
    }

    public String wijzigGegvens() {
        User user2  = this.userSession.getUser();
        this.setNieuweUser(user2);
        return "wijzigenGegevens";
    }

    public void createUser() {
        UserRol rol = (UserRol) this.userRolService.findByFunctie("user");
        this.setUserRol(rol);

        userService.createUser(getUserRol(), nieuweVoornaam, nieuweAchternaam, nieuweEmail, nieuweStraatEnHuisnummer, nieuwePostcode, nieuweGemeente, nieuwPassword);
    }

    public void updateUser() {
        userService.updateUser(nieuweUser);
    }

    public List<User> findAll() {
        return this.userService.findAllUsers();
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public UserRolService getUserRolService() {
        return userRolService;
    }

    public void setUserRolService(UserRolService userRolService) {
        this.userRolService = userRolService;
    }

    public void setWinkelmandService(WinkelmandService winkelmandService) {
        this.winkelmandService = winkelmandService;
    }

    public UserSession getUserSession() {
        return userSession;
    }

    public void setUserSession(UserSession userSession) {
        this.userSession = userSession;
    }

    public UserRol getUserRol() {
        return userRol;
    }

    public void setUserRol(UserRol userRol) {
        this.userRol = userRol;
    }

    public User getNieuweUser() {return nieuweUser;}

    public void setNieuweUser(User nieuweUser) {
        this.nieuweUser = nieuweUser;
    }

    private void SaveNieuweUser() {
        this.userService.save(getNieuweUser());
        setNieuweUser(new User());
    }

    public String getNieuweVoornaam() {
        return nieuweVoornaam;
    }

    public void setNieuweVoornaam(String nieuweVoornaam) {
        this.nieuweVoornaam = nieuweVoornaam;
    }

    public String getNieuweAchternaam() {
        return nieuweAchternaam;
    }

    public void setNieuweAchternaam(String nieuweAchternaam) {
        this.nieuweAchternaam = nieuweAchternaam;
    }

    public String getNieuweStraatEnHuisnummer() {
        return nieuweStraatEnHuisnummer;
    }

    public void setNieuweStraatEnHuisnummer(String nieuweStraatEnHuisnummer) {
        this.nieuweStraatEnHuisnummer = nieuweStraatEnHuisnummer;
    }

    public Winkelmand getWinkelmand() {
        return winkelmand;
    }
    public void setWinkelmand(Winkelmand winkelmand) {
        this.winkelmand = winkelmand;
    }

    public void setWinkelmanden(List<Winkelmand> winkelmanden) {
        this.winkelmanden = winkelmanden;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public int getNieuwePostcode() {
        return nieuwePostcode;
    }

    public void setNieuwePostcode(int nieuwePostcode) {
        this.nieuwePostcode = nieuwePostcode;
    }

    public String getNieuweGemeente() {
        return nieuweGemeente;
    }

    public void setNieuweGemeente(String nieuweGemeente) {
        this.nieuweGemeente = nieuweGemeente;
    }

    public String getNieuweEmail() {
        return nieuweEmail;
    }

    public void setNieuweEmail(String nieuweEmail) {
        this.nieuweEmail = nieuweEmail;
    }

    public String getNieuwPassword() {
        return nieuwPassword;
    }

    public void setNieuwPassword(String nieuwPassword) {
        this.nieuwPassword = nieuwPassword;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isAdmin() {
        return userSession.getUser() != null && userSession.getUser().getRol().getLevel() == 1;
    }

}
