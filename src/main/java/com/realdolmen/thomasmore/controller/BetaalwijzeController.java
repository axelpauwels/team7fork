package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.Betaalwijze;
import com.realdolmen.thomasmore.service.BetaalwijzeService;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class BetaalwijzeController {

    @ManagedProperty("#{betaalwijzeService}")
    private BetaalwijzeService betaalwijzeService;

    private String nieuweNaam;
    private Betaalwijze nieuweBetaalwijze;

    public void createBetaalwijze() {
        betaalwijzeService.createBetaalwijze(nieuweNaam);
        //_addMessage("Betaalwijze toegevoegd!");
        _clearForm();
    }

    private void _addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    private void _clearForm() {
        nieuweNaam = null;
    }

    public BetaalwijzeService getBetaalwijzeService() {
        return betaalwijzeService;
    }

    public void setBetaalwijzeService(BetaalwijzeService betaalwijzeService) {
        this.betaalwijzeService = betaalwijzeService;
    }

    public List<Betaalwijze> getBetaalwijzen(){
        /*testdata*/
        if (betaalwijzeService.getAllBetaalwijzen().size() == 0){
            betaalwijzeService.save(new Betaalwijze("mastercard"));
            betaalwijzeService.save(new Betaalwijze("visa"));
            betaalwijzeService.save(new Betaalwijze("bankcontact"));
        }
        /*einde testdata*/
        System.out.println("betaalwijzes");
        return betaalwijzeService.getAllBetaalwijzen();
    }

    public String getNieuweNaam() {
        return nieuweNaam;
    }

    public void setNieuweNaam(String nieuweNaam) {
        this.nieuweNaam = nieuweNaam;
    }

    public Betaalwijze getNieuweBetaalwijze() {
        return nieuweBetaalwijze;
    }

    public void setNieuweBetaalwijze(Betaalwijze nieuweBetaalwijze) {
        this.nieuweBetaalwijze = nieuweBetaalwijze;
    }

    private void SaveNieuweBetaalwijze() {
        this.betaalwijzeService.save(getNieuweBetaalwijze());
        setNieuweBetaalwijze(new Betaalwijze());
    }
}
