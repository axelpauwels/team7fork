package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.Boek;
import com.realdolmen.thomasmore.domain.Genre;
import com.realdolmen.thomasmore.service.GenreService;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.text.Collator;
import java.util.*;

@ManagedBean
@SessionScoped
public class GenreController {

    //Autowired kunnen we niet gebruiken in JSF, daarom gebruiken we hier dit om een spring bean te injecteren.
    @ManagedProperty("#{genreService}")
    private GenreService genreService;
    private String newGenreNaam;

    private List<Genre> genres;
    public List<Genre> getGenres() {
        if (genres == null) {
            genres = genreService.findAllGenres();
        }
        sorteerGenres();
        return genres;
    }
    /** CONSTRUCTORS */
    public GenreController() {
    }

    /** FUNCTIONS */
    public void createGenre() {
        genreService.createGenre(newGenreNaam);
        genres = genreService.findAllGenres();
        sorteerGenres();
        _addMessage("Genre toegevoegd!");
        _clearForm();
    }

    public void sorteerGenres() {
        Collator collator = Collator.getInstance(Locale.US);
        if (!genres.isEmpty()) {
            Collections.sort(genres, new Comparator<Genre>() {
                @Override
                public int compare(Genre c1, Genre c2) {
                    //You should ensure that list doesn't contain null values!
                    return collator.compare(c1.getNaam(), c2.getNaam());
                }
            });
        }
    }

    public String toonGenres(){
        return "./philip.xhtml?faces-redirect=true";
    }

    private void _addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    private void _clearForm() {
        newGenreNaam = null;
    }

    /** GETTERS & SETTERS */
    public String getNewGenreNaam() {
        return newGenreNaam;
    }

    public void setNewGenreNaam(String newGenreNaam) {
        this.newGenreNaam = newGenreNaam;
    }
    /**Deze setter MOET aanwezig zijn, anders kan spring deze service niet injecteren.*/
    public void setGenreService(GenreService genreService) {
        this.genreService = genreService;
    }
}
