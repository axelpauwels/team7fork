package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.UserRol;
import com.realdolmen.thomasmore.service.UserRolService;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.List;

@ManagedBean
@SessionScoped
public class UserRolController {

    @ManagedProperty("#{userRolService}")
    private UserRolService userRolService;

    private UserRol nieuweUserRol;
    private int nieuwLevel;
    private String nieuweFunctie;

    public void createUserRol() {
        userRolService.createUserRol(nieuwLevel, nieuweFunctie);
        //_addMessage("Rol toegevoegd!");
        _clearForm();
    }

    private void _addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    private void _clearForm() {
        nieuwLevel = 0;
        nieuweFunctie = null;
    }

    public String toonRollen(){
        return "./overzichtRollen.xhtml?faces-redirect=true";
    }

    public List<UserRol> findAll() {
        return this.userRolService.findAllRollen();
    }

    public List<UserRol> getRollen(){
        return userRolService.findAllRollen();
    }

    public UserRolService getUserRolService() {
        return userRolService;
    }

    public void setUserRolService(UserRolService userRolService) {
        this.userRolService = userRolService;
    }

    public UserRol getNieuweUserRol() {
        return nieuweUserRol;
    }

    public void setNieuweUserRol(UserRol nieuweUserRol) {
        this.nieuweUserRol = nieuweUserRol;
    }

    public void SaveNieuweUserRol() {
        this.userRolService.save(getNieuweUserRol());
        setNieuweUserRol(new UserRol());
    }

    public int getNieuwLevel() {
        return nieuwLevel;
    }

    public void setNieuwLevel(int nieuwLevel) {
        this.nieuwLevel = nieuwLevel;
    }

    public String getNieuweFunctie() {
        return nieuweFunctie;
    }

    public void setNieuweFunctie(String nieuweFunctie) {
        this.nieuweFunctie = nieuweFunctie;
    }
}
