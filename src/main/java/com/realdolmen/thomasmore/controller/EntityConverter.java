package com.realdolmen.thomasmore.controller;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.HashMap;
import java.util.Map;

@ManagedBean
@ViewScoped
public class EntityConverter implements Converter {

    final private Map<String, Object> converterMap = new HashMap<String, Object>();
    final private Map<Object, String> reverseConverterMap = new HashMap<Object, String>();

    private int objectCounter = 1;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
            return this.converterMap.get(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (reverseConverterMap.containsKey(value)) {
            return reverseConverterMap.get(value);
        } else {
            String objectId = String.valueOf(objectCounter++);
            converterMap.put(objectId, value);
            reverseConverterMap.put(value, objectId);
            return objectId;
        }
    }
}