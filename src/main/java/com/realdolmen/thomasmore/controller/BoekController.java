package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.Auteur;
import com.realdolmen.thomasmore.domain.Boek;
import com.realdolmen.thomasmore.domain.Genre;
import com.realdolmen.thomasmore.domain.Uitgeverij;
import com.realdolmen.thomasmore.service.AuteurService;
import com.realdolmen.thomasmore.service.BoekService;
import com.realdolmen.thomasmore.service.GenreService;
import com.realdolmen.thomasmore.service.UitgeverijService;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.text.Collator;
import java.util.*;

@ManagedBean
@SessionScoped
public class BoekController {

    //Autowired kunnen we niet gebruiken in JSF, daarom gebruiken we hier dit om een spring bean te injecteren.
    @ManagedProperty("#{boekService}")
    private BoekService boekService;
    @ManagedProperty("#{genreService}")
    private GenreService genreService;
    @ManagedProperty("#{auteurService}")
    private AuteurService auteurService;
    @ManagedProperty("#{uitgeverijService}")
    private UitgeverijService uitgeverijService;
    private Boek boek;

    private List<Boek> boeken;
    public List<Boek> getBoeken() {
        if (boeken == null) {
            boeken = boekService.findAllBoeken();
        }
//        sorteerBoeken();
        return boeken;
    }

    private String newBoekTitel;
    private String newBoekBeschrijving;
    private double newBoekPrijs;
    private Genre newBoekGenre;
    private Auteur newBoekAuteur;
    private Uitgeverij newBoekUitgeverij;
    private int newBoekVoorraad;

    /** CONSTRUCTORS */
    public BoekController() {
    }

    /** FUNCTIONS */
    public void createBoek() {
        boekService.createBoek(newBoekTitel, newBoekBeschrijving, newBoekPrijs, newBoekGenre, newBoekAuteur, newBoekUitgeverij, newBoekVoorraad);
        sorteerBoeken();
        _addMessage("Boek toegevoegd!");
        _clearForm();
    }

    public void sorteerBoeken() {
        Collator collator = Collator.getInstance(Locale.US);
        if (!boeken.isEmpty()) {
            Collections.sort(boeken, new Comparator<Boek>() {
                @Override
                public int compare(Boek c1, Boek c2) {
                    //You should ensure that list doesn't contain null values!
                    return collator.compare(c1.getTitel(), c2.getTitel());
                }
            });
        }
    }

    public String toonBoeken(){
        return "./axel.xhtml?faces-redirect=true";
    }
    public String addBoeken(){
        return "./axelAddBoeken.xhtml?faces-redirect=true";
    }

    public String lijstBoeken() {
        List<Boek> alleboeken = boekService.findAllBoeken();
        setBoeken(alleboeken);
        return "/test/lijstBoeken.xhtml?faces-redirect=true";
    }

    public List<Boek> getAllBoeken() {
        return boekService.findAllBoeken();
    }

    public Boek getBoekFromParam() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if(params.get("boekId") != null) {
            boek = boekService.findById(Long.parseLong(params.get("boekId")));
        }
        return boek;
    }

    //public String boekDetails() {
    //    Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    //    if(params.get("boekId") != null) {
    //        setBoek(boekService.findById(Long.parseLong("boekId")));
    //        return "/test/boek.xhtml";
    //    } else {
    //        return "404";
    //    }
    //}

    public void _clearForm() {
        newBoekTitel = null;
        newBoekBeschrijving = null;
        newBoekPrijs = 0.0;
        newBoekGenre = null;
        newBoekAuteur = null;
        newBoekUitgeverij = null;
        newBoekVoorraad = 0;
    }

    private void _addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /** GETTERS & SETTERS */
    public String getNewBoekTitel() {
        return newBoekTitel;
    }

    public void setNewBoekTitel(String newBoekTitel) {
        this.newBoekTitel = newBoekTitel;
    }

    public String getNewBoekBeschrijving() {
        return newBoekBeschrijving;
    }

    public void setNewBoekBeschrijving(String newBoekBeschrijving) {
        this.newBoekBeschrijving = newBoekBeschrijving;
    }

    public double getNewBoekPrijs() {
        return newBoekPrijs;
    }

    public void setNewBoekPrijs(double newBoekPrijs) {
        this.newBoekPrijs = newBoekPrijs;
    }

    public Genre getNewBoekGenre() {
        return newBoekGenre;
    }

    public void setNewBoekGenre(Genre newBoekGenre) {
        this.newBoekGenre = newBoekGenre;
    }

    public Auteur getNewBoekAuteur() {
        return newBoekAuteur;
    }

    public void setNewBoekAuteur(Auteur newBoekAuteur) {
        this.newBoekAuteur = newBoekAuteur;
    }

    public Uitgeverij getNewBoekUitgeverij() {
        return newBoekUitgeverij;
    }

    public void setNewBoekUitgeverij(Uitgeverij newBoekUitgeverij) {
        this.newBoekUitgeverij = newBoekUitgeverij;
    }

    public int getNewBoekVoorraad() {
        return newBoekVoorraad;
    }

    public void setNewBoekVoorraad(int newBoekVoorraad) {
        this.newBoekVoorraad = newBoekVoorraad;
    }

    public Boek getBoek() {
        return boek;
    }

    public void setBoek(Boek boek) {
        this.boek = boek;
    }

    public void setBoeken(List<Boek> boeken) {
        this.boeken = boeken;
    }

    /**Deze setter MOET aanwezig zijn, anders kan spring deze service niet injecteren.*/
    public void setBoekService(BoekService boekService) {
        this.boekService = boekService;
    }
    public void setGenreService(GenreService genreService) {
        this.genreService = genreService;
    }
    public void setAuteurService(AuteurService auteurService) {
        this.auteurService = auteurService;
    }
    public void setUitgeverijService(UitgeverijService uitgeverijService) { this.uitgeverijService = uitgeverijService; }

}
