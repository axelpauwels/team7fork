package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.Genre;
import com.realdolmen.thomasmore.domain.Uitgeverij;
import com.realdolmen.thomasmore.service.UitgeverijService;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

@ManagedBean
@SessionScoped
public class UitgeverijController {
    // Declareren
    @ManagedProperty("#{uitgeverijService}")
    private UitgeverijService uitgeverijService;
    private Uitgeverij uitgeverij;
    private String newUitgeverijNaam;
    private String newUitgeverijBeschrijving;

    private List<Uitgeverij> uitgeverijen;
    public List<Uitgeverij> getUitgeverijen() {
        if (uitgeverijen == null) {
            uitgeverijen = uitgeverijService.findAllUitgeverijen();
        }
        sorteerUitgeverijen();
        return uitgeverijen;
    }

    // Constructor
    public UitgeverijController(){
    }

    // Methodes
    public void createUitgeverij() {
        uitgeverijService.createUitgeverij(newUitgeverijNaam, newUitgeverijBeschrijving);
        uitgeverijen = uitgeverijService.findAllUitgeverijen();
//        sorteerUitgeverijen();
        _addMessage("Uitgeverij toegevoegd!");
        _clearForm();
    }

    public void sorteerUitgeverijen() {
        Collator collator = Collator.getInstance(Locale.US);
        if (!uitgeverijen.isEmpty()) {
            Collections.sort(uitgeverijen, new Comparator<Uitgeverij>() {
                @Override
                public int compare(Uitgeverij c1, Uitgeverij c2) {
                    //You should ensure that list doesn't contain null values!
                    return collator.compare(c1.getNaam(), c2.getNaam());
                }
            });
        }
    }


    public String toonUitgeverijen(){ return "./uitgeverijOverzicht.xhtml?faces-redirect=true"; }

    private void _addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    private void _clearForm() {
        newUitgeverijNaam = null;
        newUitgeverijBeschrijving = null;
    }

    // Getters & Setters
    public UitgeverijService getUitgeverijService() { return uitgeverijService; }

    public void setUitgeverijService(UitgeverijService uitgeverijService) { this.uitgeverijService = uitgeverijService; }

    public Uitgeverij getUitgeverij() { return uitgeverij; }

    public void setUitgeverij(Uitgeverij uitgeverij) { this.uitgeverij = uitgeverij; }

    public String getNewUitgeverijNaam() { return newUitgeverijNaam; }

    public void setNewUitgeverijNaam(String newUitgeverijNaam) { this.newUitgeverijNaam = newUitgeverijNaam; }

    public String getNewUitgeverijBeschrijving() { return newUitgeverijBeschrijving; }

    public void setNewUitgeverijBeschrijving(String newUitgeverijBeschrijving) { this.newUitgeverijBeschrijving = newUitgeverijBeschrijving; }
}
