package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.authentication.UserSession;
import com.realdolmen.thomasmore.domain.*;
//import com.realdolmen.thomasmore.service.ItemService;
import com.realdolmen.thomasmore.service.UserService;
import com.realdolmen.thomasmore.service.WinkelmandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.List;

@ManagedBean
@SessionScoped
public class WinkelmandController {

    @ManagedProperty("#{winkelmandService}")
    private WinkelmandService winkelmandService;
    
    @ManagedProperty("#{userSession}")
    private UserSession userSession;

    //@ManagedProperty("#{itemService}")
    //private ItemService itemService;

    private User user; //voor te weten van wie de mand is

    private Item nieuwItem;

    private Winkelmand huidigeWinkelmand;

    public WinkelmandService getWinkelmandService() {
        return winkelmandService;
    }

    public void setWinkelmandService(WinkelmandService winkelmandService) {
        this.winkelmandService = winkelmandService;
    }

    public void setUserSession(UserSession userSession) {
        this.userSession = userSession;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Item getNieuwItem() {
        if(nieuwItem == null) {
            nieuwItem = new Item();
            nieuwItem.setAantal(1);
        }
        return nieuwItem;
    }

    public void setNieuwItem(Item nieuwItem) {
        this.nieuwItem = nieuwItem;
    }

    public List<Winkelmand> getBestellingen() {
        return winkelmandService.getBestellingen();
    }

    public Winkelmand getHuidigeWinkelmand() {
        if(huidigeWinkelmand == null) {
            return winkelmandService.getHuidigeWinkelmand(getUser());
        } else {
            return huidigeWinkelmand;
        }
    }

    public void setHuidigeWinkelmand(Winkelmand huidigeWinkelmand) {
        this.huidigeWinkelmand = huidigeWinkelmand;
    }

    public void saveHuidigeWinkelmand(){
        winkelmandService.save(getHuidigeWinkelmand());
    }

    private void bestel() {
        Winkelmand winkelmand = winkelmandService.getHuidigeWinkelmand(getUser());
        winkelmand.setBesteld(true);
        winkelmandService.save(winkelmand);
    }

    private void delete(){
        if (huidigeWinkelmand != null) {
            winkelmandService.delete(huidigeWinkelmand);
        }
    }

    public void addItem(Boek b) {
        if(huidigeWinkelmand == null) {
            huidigeWinkelmand = new Winkelmand();
            System.out.println("usersession is " + userSession);
            if (userSession != null && userSession.getUser() != null) {
                huidigeWinkelmand.setUser(userSession.getUser());
            }
            huidigeWinkelmand.setBesteld(false);
            winkelmandService.save(huidigeWinkelmand);
        }
        nieuwItem.setBoek(b);
        huidigeWinkelmand.addItem(nieuwItem);
        winkelmandService.save(huidigeWinkelmand);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Boek \"" + b.getTitel() + "\" toegevoegd."));
        nieuwItem = new Item(); //item resetten.
        nieuwItem.setAantal(1);
        winkelmandService.save(huidigeWinkelmand);
    }

    public String getAantalInMandNav() {
        if(huidigeWinkelmand == null || huidigeWinkelmand.getItems().size() == 0) {
            return "";
        } else {
            return "(" + huidigeWinkelmand.getItems().size() + ")";
        }
    }
}
