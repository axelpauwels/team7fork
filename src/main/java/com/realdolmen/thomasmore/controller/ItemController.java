package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.Boek;
import com.realdolmen.thomasmore.domain.Item;
import com.realdolmen.thomasmore.domain.Winkelmand;
import com.realdolmen.thomasmore.service.ItemService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.util.List;

@ManagedBean
@SessionScoped
public class ItemController {
    // Declareren
    @ManagedProperty("#{itemService}")
    private ItemService itemService;
    private Item item;
    private Boek newBoek;
    private int newAantal;
    private Winkelmand newWinkelmand;
    public List<Item> getItems() { return itemService.findAllItems(); }

    // Constructor
    public ItemController(){
    }

    // Methodes

    public void createItem() { itemService.createItem(newBoek, newAantal, newWinkelmand);}

   // public String toonItems(){ return "./itemOverzicht.xhtml"; }

    public String toonItems(){ return "./itemOverzicht.xhtml?faces-redirect=true"; }

    // Getters & Setters
    public ItemService getItemService() { return itemService; }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    public Item getItem() { return item; }

    public void setItem(Item item) { this.item = item; }

    public Boek getNewBoek() { return newBoek; }

    public void setNewBoek(Boek newBoek) { this.newBoek = newBoek; }

    public int getNewAantal() { return newAantal; }

    public void setNewAantal(int newAantal) { this.newAantal = newAantal; }

    public Winkelmand getNewWinkelmand() { return newWinkelmand; }

    public void setNewWinkelmand(Winkelmand newWinkelmand) { this.newWinkelmand = newWinkelmand; }
}
