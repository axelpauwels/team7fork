package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.Auteur;
import com.realdolmen.thomasmore.domain.User;
import com.realdolmen.thomasmore.service.AuteurService;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

@ManagedBean
@SessionScoped
public class AuteurController {

    //Autowired kunnen we niet gebruiken in JSF, daarom gebruiken we hier dit om een spring bean te injecteren.
    @ManagedProperty("#{auteurService}")
    private AuteurService auteurService;
    private String newAuteursNaam;
    private List<Auteur> auteurs;
    public List<Auteur> getAuteurs() {
        if (auteurs == null) {
            auteurs = auteurService.findAllAuteurs();
        }
        sorteerAuteurs();
        return auteurs;
    }

    /** CONSTRUCTORS */
    public AuteurController() {
    }



    /** FUNCTIONS */
    public void createAuteur() {
        auteurService.createAuteur(newAuteursNaam);
        auteurs = auteurService.findAllAuteurs();
        sorteerAuteurs();
        _addMessage("Auteur toegevoegd!");
        _clearForm();
    }

    public void sorteerAuteurs() {
        Collator collator = Collator.getInstance(Locale.US);
        if (!auteurs.isEmpty()) {
            Collections.sort(auteurs, new Comparator<Auteur>() {
                @Override
                public int compare(Auteur c1, Auteur c2) {
                    //You should ensure that list doesn't contain null values!
                    return collator.compare(c1.getNaam(), c2.getNaam());
                }
            });
        }
    }

    public String toonAuters(){
        return "./auteurOverzicht.xhtml?faces-redirect=true";
    }

    private void _addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage. SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }



    private void _clearForm() {
        newAuteursNaam = null;
    }

    /** GETTERS & SETTERS */
    public String getNewAuteursNaam() {
        return newAuteursNaam;
    }

    public void setNewAuteursNaam(String newAuteursNaam) {
        this.newAuteursNaam = newAuteursNaam;
    }


    /**Deze setter MOET aanwezig zijn, anders kan spring deze service niet injecteren.*/
    public void setAuteurService(AuteurService auteurService) {
        this.auteurService = auteurService;
    }
}