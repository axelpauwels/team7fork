package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.authentication.UserSession;
import com.realdolmen.thomasmore.domain.Betaalwijze;
import com.realdolmen.thomasmore.domain.SupportTicket;
import com.realdolmen.thomasmore.domain.User;
import com.realdolmen.thomasmore.service.SupportTicketService;
import com.realdolmen.thomasmore.service.UserService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.util.GregorianCalendar;
import java.util.List;

@ManagedBean
@SessionScoped
public class SupportTicketController {

    @ManagedProperty("#{supportTicketService}")
    private SupportTicketService supportTicketService;

    @ManagedProperty("#{userService}")
    private UserService userService;

    @ManagedProperty("#{userSession}")
    private UserSession userSession;

    private User user; //voor te weten van wie het ticket is
    private SupportTicket nieuwSupportTicket;
    private String nieuwOnderwerp;
    private String nieuweInhoud;

    public void createSupportTicket() {
        GregorianCalendar nieuweDatum = new GregorianCalendar();
        this.setUser(userSession.getUser());

        supportTicketService.createSupportTicket(getUser(), nieuwOnderwerp, nieuweInhoud, nieuweDatum);
    }

    public void updateSupportTicket(Long ticketId) {
        SupportTicket ticket = supportTicketService.findById(ticketId);
        ticket.setIsAfgehandeld(true);

        supportTicketService.updateSupportTicket(ticket);
    }

    public List<SupportTicket> getTickets(){
        return supportTicketService.findAllTickets();
    }

    public List<SupportTicket> getTicketsByUser(){
        this.setUser(userSession.getUser());
        return supportTicketService.findAllByUser(user);
    }

    public SupportTicketService getSupportTicketService() {
        return supportTicketService;
    }

    public void setSupportTicketService(SupportTicketService supportTicketService) {
        this.supportTicketService = supportTicketService;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public UserSession getUserSession() {
        return userSession;
    }

    public void setUserSession(UserSession userSession) {
        this.userSession = userSession;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public SupportTicket getNieuwSupportTicket() {
        return nieuwSupportTicket;
    }

    public void setNieuwSupportTicket(SupportTicket nieuwSupportTicket) {
        this.nieuwSupportTicket = nieuwSupportTicket;
    }

    private void SaveNieuwSupportTicket() {
        this.supportTicketService.save(getNieuwSupportTicket());
        setNieuwSupportTicket(new SupportTicket());
    }

    public String getNieuwOnderwerp() {
        return nieuwOnderwerp;
    }

    public void setNieuwOnderwerp(String nieuwOnderwerp) {
        this.nieuwOnderwerp = nieuwOnderwerp;
    }

    public String getNieuweInhoud() {
        return nieuweInhoud;
    }

    public void setNieuweInhoud(String nieuweInhoud) {
        this.nieuweInhoud = nieuweInhoud;
    }
}
