package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.Auteur;
import com.realdolmen.thomasmore.domain.Boek;
import com.realdolmen.thomasmore.domain.Genre;
import com.realdolmen.thomasmore.domain.Uitgeverij;
import com.realdolmen.thomasmore.repository.AuteurRepository;
import com.realdolmen.thomasmore.repository.BoekRepository;
import com.realdolmen.thomasmore.repository.GenreRepository;
import com.realdolmen.thomasmore.repository.UitgeverijRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BoekService {

    @Autowired
    private BoekRepository boekRepository;
    @Autowired
    private GenreRepository genreRepository;
    @Autowired
    private AuteurRepository auteurRepository;
    @Autowired
    private UitgeverijRepository uitgeverijRepository;

    public void createBoek(String titel, String beschrijving, double prijs, Genre genre, Auteur auteur, Uitgeverij uitgeverij, int voorraad) {
            Boek boek = new Boek();
            boek.setTitel(titel);
            boek.setBeschrijving(beschrijving);
            boek.setPrijs(prijs);
            boek.setGenre(genre);
            boek.setAuteur(auteur);
            boek.setUitgeverij(uitgeverij);
            boek.setVoorraad(voorraad);
            boekRepository.save(boek);

//        if (titel !=null && titel != "" && beschrijving != null && beschrijving != "" && prijs != 0 && genre != null && auteur !=null &&  uitgeverij !=null  && voorraad !=0   ){
//            Boek boek = new Boek();
//            boek.setTitel(titel);
//            boek.setBeschrijving(beschrijving);
//            boek.setPrijs(prijs);
//            boek.setGenre(genre);
//            boek.setAuteur(auteur);
//            boek.setUitgeverij(uitgeverij);
//            boek.setVoorraad(voorraad);
//            boekRepository.save(boek);
//        }
    }

    public void createDummyData() {
        Boek boek1 = new Boek();
        Genre genre1 = genreRepository.findGenreById(1);
        Auteur auteur1 = auteurRepository.findAuteurById(1);
        Uitgeverij uitgeverij1 = uitgeverijRepository.findUitgeverijById(1);

        boek1.setTitel("Het zachte schuurpapier");
        boek1.setBeschrijving("Dit boek is gebaseerd op een waargebeurd verhaal.");
        boek1.setPrijs(33.50);
        boek1.setGenre(genre1);
        boek1.setAuteur(auteur1);
        boek1.setUitgeverij(uitgeverij1);
        boek1.setVoorraad(8);
        boekRepository.save(boek1);

        Boek boek2 = new Boek();
        Genre genre2 = genreRepository.findGenreById(2);
        Auteur auteur2 = auteurRepository.findAuteurById(2);
        Uitgeverij uitgeverij2 = uitgeverijRepository.findUitgeverijById(2);
        boek2.setTitel("Het gebroken glaasje");
        boek2.setBeschrijving("Komt met een fles wijn.");
        boek2.setPrijs(51.25);
        boek2.setGenre(genre2);
        boek2.setAuteur(auteur2);
        boek2.setUitgeverij(uitgeverij2);
        boek2.setVoorraad(15);
        boekRepository.save(boek2);
    }

    public Boek findById(Long id) {
        if(id != 0){
            return boekRepository.findBoekById(id);
        }
        return null;
    }

    public List<Boek> findAllBoeken() {
        return boekRepository.findAll();
    }

    public List<Boek> findByPrijs(double prijs) {
        if(prijs != 0 ){
            return boekRepository.findByPrijs(prijs);
        }
        return null;
    }

    public List<Boek> findByAuteur(Auteur auteur) {
        if(auteur != null ){
            return boekRepository.findByAuteur(auteur);
        }
        return null;
    }

    public List<Boek> findByGenre(Genre genre) {
        if(genre != null ){
            return boekRepository.findByGenre(genre);
        }
        return null;
    }

    public List<Boek> findByUitgeverij(Uitgeverij uitgeverij) {
        if(uitgeverij != null ){
            return boekRepository.findByUitgeverij(uitgeverij);
        }
        return null;
    }

    public List<Boek> findByVoorraadBetween(int voorraadMin, int voorraadMax) {
        if(voorraadMin >= 0 && voorraadMax != 0){
            return boekRepository.findByVoorraadBetween(voorraadMin,voorraadMax);
        }
        return null;
    }


}
