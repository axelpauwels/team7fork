package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.UserRol;
import com.realdolmen.thomasmore.repository.UserRolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRolService {

    @Autowired
    private UserRolRepository userRolRepository;

    public void createUserRol(int level, String functie) {
        UserRol rol = new UserRol();
        rol.setLevel(level);
        rol.setFunctie(functie);

        userRolRepository.save(rol);
    }

    public void createDummyData() {
        UserRol userRol = new UserRol();
        userRol.setLevel(1);
        userRol.setFunctie("admin");
        userRolRepository.save(userRol);

        UserRol userRol2 = new UserRol();
        userRol2.setLevel(2);
        userRol2.setFunctie("user");
        userRolRepository.save(userRol2);
    }

    public List<UserRol> findAllRollen() {
        return userRolRepository.findAll();
    }

    public UserRol findById(Long id) { return userRolRepository.findById(id); }

    public UserRol findByLevel(int level) { return userRolRepository.findByLevel(level); }

    public UserRol findByFunctie(String functie) { return userRolRepository.findByFunctie(functie); }

    public void save(UserRol rol) {
        userRolRepository.save(rol);
    }
}
