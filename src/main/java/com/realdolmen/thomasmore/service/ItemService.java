package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.Boek;
import com.realdolmen.thomasmore.domain.Item;
import com.realdolmen.thomasmore.domain.Winkelmand;
import com.realdolmen.thomasmore.repository.BoekRepository;
import com.realdolmen.thomasmore.repository.ItemRepository;
import com.realdolmen.thomasmore.repository.WinkelmandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemService {

    @Autowired
    private  ItemRepository itemRepository;
    @Autowired
    private BoekRepository boekRepository;
    @Autowired
    private WinkelmandRepository winkelmandRepository;

    public void createItem(Boek boek, int aantal, Winkelmand winkelmand){
        Item item = new Item();
        item.setBoek(boek);
        item.setAantal(aantal);
        item.setWinkelmand(winkelmand);

        itemRepository.save(item);
    }

    public void deleteItem(long itemId){
        System.out.println("****deleteItemService");
        itemRepository.delete(itemId);
    }

    public void createDummyData(){
        Item item1 = new Item();
        Item item2 = new Item();
        Boek boek1 = boekRepository.findBoekById(1);
        Boek boek2 = boekRepository.findBoekById(2);
        Winkelmand winkelmand = winkelmandRepository.findById(1);

        item1.setBoek(boek1);
        item1.setAantal(1);
        item1.setWinkelmand(winkelmand);
        itemRepository.save(item1);

        item2.setBoek(boek2);
        item2.setAantal(2);
        item2.setWinkelmand(winkelmand);
        itemRepository.save(item2);
    }

    public List<Item> findAllItems() {
        return itemRepository.findAll();
    }
    public void updateItem(Item item) {itemRepository.save(item);}
}
