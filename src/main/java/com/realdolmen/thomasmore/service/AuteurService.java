package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.Auteur;
import com.realdolmen.thomasmore.repository.AuteurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuteurService {

    @Autowired
    private AuteurRepository auteurRepository;

    public void createAuteur(String naam) {
        Auteur auteur = new Auteur();
        auteur.setNaam(naam);

        auteurRepository.save(auteur);
    }
    public void createDummyData() {
        Auteur auteur1 = new Auteur();
        auteur1.setNaam("Roald Dahl");
        auteurRepository.save(auteur1);

        Auteur auteur2 = new Auteur();
        auteur2.setNaam("Dikkie Dik");
        auteurRepository.save(auteur2);
    }

    public List<Auteur> findAllAuteurs() {
        return auteurRepository.findAll();
    }
    public Auteur findAuteurById(long id){
        return auteurRepository.findAuteurById(id);
    }


}
