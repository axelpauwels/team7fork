package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.Uitgeverij;
import com.realdolmen.thomasmore.repository.UitgeverijRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UitgeverijService {

    @Autowired
    private  UitgeverijRepository uitgeverijRepository;

    public void createUitgeverij(String naam, String beschrijving){
        Uitgeverij uitgeverij = new Uitgeverij();
        uitgeverij.setNaam(naam);
        uitgeverij.setBeschrijving(beschrijving);
        uitgeverijRepository.save(uitgeverij);
    }

    public void createDummyData() {
        Uitgeverij uitgeverij1 = new Uitgeverij();
        uitgeverij1.setNaam("De uitgeverij");
        uitgeverij1.setBeschrijving("De enige echte uitgeverij die er toe doet.");
        uitgeverijRepository.save(uitgeverij1);

        Uitgeverij uitgeverij2 = new Uitgeverij();
        uitgeverij2.setNaam("De boekgeverij");
        uitgeverij2.setBeschrijving("Beste boek slinger van het land.");
        uitgeverijRepository.save(uitgeverij2);
    }

    public List<Uitgeverij> findAllUitgeverijen() {
        return uitgeverijRepository.findAll();
    }
    public Uitgeverij findUitgeverijById(long id){
        return uitgeverijRepository.findUitgeverijById(id);
    }
}
