package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.Genre;
import com.realdolmen.thomasmore.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreService {

    @Autowired
    private GenreRepository genreRepository;

    public void createGenre(String naam) {
        Genre genre = new Genre();
        genre.setNaam(naam);
        genreRepository.save(genre);
    }

    public void createDummyData() {
        Genre genre1 = new Genre();
        genre1.setNaam("Horror");
        genreRepository.save(genre1);

        Genre genre2 = new Genre();
        genre2.setNaam("Avontuur");
        genreRepository.save(genre2);
    }


    public List<Genre> findAllGenres() {
        return genreRepository.findAll();
    }

    public Genre findGenreById(long id){
        return genreRepository.findGenreById(id);
    }

}
