package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.*;
import com.realdolmen.thomasmore.repository.*;
import org.hibernate.internal.util.compare.CalendarComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.ToLongFunction;

@Service
public class WinkelmandService {

    @Autowired
    private WinkelmandRepository winkelmandRepository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private BoekRepository boekRepository;
    @Autowired
    private AuteurRepository auteurRepository;
    @Autowired
    private BetaalwijzeRepository betaalwijzeRepository;
    @Autowired
    private UserRepository userRepository;

    public List<Winkelmand> getAllWinkelmanden(){
        return winkelmandRepository.findAll();
    }

    public List<Winkelmand> getAllWinkelmandenByUser(User user){
        return winkelmandRepository.findAllByUser(user);
    }
    public Winkelmand getWinkelmandById(long id){
        return winkelmandRepository.findById(id);
    }
    public List<Winkelmand> getAllWinkelmandenByDatum(GregorianCalendar startDatum,GregorianCalendar eindDatum){
        return winkelmandRepository.findAllByDatumBetween(startDatum,eindDatum);
    }

    public List<GregorianCalendar> getDatumsVanBestellingen(){
        // alle winkelmanden ophalen
        List<Winkelmand> winkelmanden = this.getAllWinkelmanden();
        List<GregorianCalendar> winkelmandenDatums = new ArrayList<>();

        // datums van alle winkelmanden opslaan in array "winkelmandenDatums" en sorteren
        for (Winkelmand winkelmand:winkelmanden) {
            winkelmandenDatums.add(winkelmand.getDatum());
        }
        Collections.sort(winkelmandenDatums, new CalendarComparator());

        // enkel de unieke datums opslaan in array uniekeDatums"
        List<GregorianCalendar> uniekeDatums = new ArrayList<>();
        uniekeDatums.add(winkelmandenDatums.get(0));
        GregorianCalendar vorigeDatum = winkelmandenDatums.get(0);

        for (GregorianCalendar winkelmandenDatum:winkelmandenDatums) {
            if(vorigeDatum.get(Calendar.YEAR) == winkelmandenDatum.get(Calendar.YEAR) && vorigeDatum.get(Calendar.MONTH) == winkelmandenDatum.get(Calendar.MONTH) && vorigeDatum.get(Calendar.DAY_OF_MONTH) == winkelmandenDatum.get(Calendar.DAY_OF_MONTH)) {
//                System.out.println("Deze datum bestaat al in de list 'uniekeDatums' ");
            }else if(!uniekeDatums.contains(winkelmandenDatum)){
                uniekeDatums.add(winkelmandenDatum);
                vorigeDatum = winkelmandenDatum;
            }
        }
        return uniekeDatums;
//        return winkelmandenDatums;
    }

    //  Custom comparator
    static class CalendarComparator implements Comparator {
        public int compare(Object object1, Object object2) { //  Method for implementing an interface
            GregorianCalendar p1 = (GregorianCalendar) object1; //  Mandatory conversion
            GregorianCalendar p2 = (GregorianCalendar) object2;
            return p2.compareTo(p1);
        }
    }

    public Winkelmand getHuidigeWinkelmand(User user){
        List<Winkelmand> winkelmanden = winkelmandRepository.findAllByUserAndIsBesteld(user,false);
        if (winkelmanden.size() == 0){
            return winkelmandRepository.save(new Winkelmand());
        } else if (winkelmanden.size() == 1) {
            return winkelmanden.get(0);
        } else if (user == null) {
            //manden zonder user verwijderen
            winkelmandRepository.deleteAllByUser(null);
            return winkelmandRepository.save(new Winkelmand());
        } else {
            throw new IllegalStateException("Het aantal huidige onbestelde winkelmanden van een klant mag niet meer als 1 zijn." +
            "\n aantal gevonden manden voor " + user.getVoornaam() + " " + user.getAchternaam()  + " is " + winkelmanden.size());
        }
    }

    public List<Winkelmand> getBestellingen(){
        return winkelmandRepository.findAllByIsBesteld(true);
    }
    public List<Winkelmand> getBestellingenByUser(User user){
        return winkelmandRepository.findAllByUserAndIsBesteld(user,true);
    }

    public void save(Winkelmand winkelmand){
        winkelmandRepository.save(winkelmand);
    }

    public void delete(Winkelmand winkelmand) {
        winkelmandRepository.delete(winkelmand);
    }

    public void generateTestData() {
        System.out.println("generateTestData");
        //if(winkelmandRepository.findAll().size() == 0) {
            System.out.println("in if");
            //direct saven voor id
            Winkelmand winkelmand = winkelmandRepository.save(new Winkelmand());

            Item item = itemRepository.save(new Item());
            Boek boek = boekRepository.save(new Boek());
            boek.setTitel("pater fictie");
            boek.setPrijs(14);
            Auteur a = auteurRepository.findAuteurById(Long.valueOf(1));
            boek.setAuteur(a);

            item.setBoek(boekRepository.save(boek));
            item.setAantal(1);
            //winkelmand.addItem(itemRepository.save(item));
            item.setWinkelmand(winkelmand);
            itemRepository.save(item);

            item = itemRepository.save(new Item());
            boek = boekRepository.save(new Boek());
            boek.setTitel("de zilveren gids");
            boek.setPrijs(15.2);

            boek.setAuteur(a);
            //boek = boekRepository.save(boek);
            item.setBoek(boekRepository.save(boek));
            item.setAantal(2);
            //winkelmand.addItem(itemRepository.save(item));
            item.setWinkelmand(winkelmand);
            itemRepository.save(item);

            winkelmand = winkelmandRepository.findById(winkelmand.getId());
            winkelmand.setBetaalwijze(betaalwijzeRepository.save(new Betaalwijze("cash")));
            winkelmand.setDatum(new GregorianCalendar());
            winkelmand.setBesteld(true);

            User u = userRepository.findById(Long.valueOf(1));
            winkelmand.setUser(userRepository.save(u));
            winkelmandRepository.save(winkelmand);

            //2de voor dezelfde user
            Winkelmand winkelmand2 = winkelmandRepository.save(new Winkelmand());

            Item item2 = itemRepository.save(new Item());
            Boek boek2 = boekRepository.save(new Boek());
            boek2.setTitel("De ongezonde");
            boek2.setPrijs(16);
            Auteur a2 = auteurRepository.findAuteurById(Long.valueOf(2));
            boek2.setAuteur(a2);

            item2.setBoek(boekRepository.save(boek2));
            item2.setAantal(3);
            //winkelmand.addItem(itemRepository.save(item));
            item2.setWinkelmand(winkelmand2);
            itemRepository.save(item2);

            item2 = itemRepository.save(new Item());
            boek2 = boekRepository.save(new Boek());
            boek2.setTitel("de gouden gids");
            boek2.setPrijs(15.2);

            boek2.setAuteur(a2);
            //boek = boekRepository.save(boek);
            item2.setBoek(boekRepository.save(boek2));
            item2.setAantal(4);
            //winkelmand.addItem(itemRepository.save(item));
            item2.setWinkelmand(winkelmand2);
            itemRepository.save(item2);

            winkelmand2 = winkelmandRepository.findById(winkelmand2.getId());
            winkelmand2.setBetaalwijze(betaalwijzeRepository.save(new Betaalwijze("cash")));
            winkelmand2.setDatum(new GregorianCalendar());
            winkelmand2.setBesteld(true);
            User u2 = userRepository.findById(Long.valueOf(1));
            winkelmand.setUser(userRepository.save(u2));
            winkelmandRepository.save(winkelmand2);


            //3de voor een andere user
            Winkelmand winkelmand3 = winkelmandRepository.save(new Winkelmand());

            Item item3 = itemRepository.save(new Item());
            Boek boek3 = boekRepository.save(new Boek());
            boek3.setTitel("Het zacht broodje");
            boek3.setPrijs(22);
            Auteur a3 = auteurRepository.findAuteurById(Long.valueOf(1));
            boek3.setAuteur(a3);

            item3.setBoek(boekRepository.save(boek3));
            item3.setAantal(3);
            //winkelmand.addItem(itemRepository.save(item));
            item3.setWinkelmand(winkelmand3);
            itemRepository.save(item3);

            item3 = itemRepository.save(new Item());
            boek3 = boekRepository.save(new Boek());
            boek3.setTitel("de witte gids");
            boek3.setPrijs(15.2);

            boek3.setAuteur(a3);
            //boek = boekRepository.save(boek);
            item3.setBoek(boekRepository.save(boek3));
            item3.setAantal(4);
            //winkelmand.addItem(itemRepository.save(item));
            item2.setWinkelmand(winkelmand2);
            itemRepository.save(item2);

            winkelmand3 = winkelmandRepository.findById(winkelmand3.getId());
            winkelmand3.setBetaalwijze(betaalwijzeRepository.save(new Betaalwijze("visa")));
            winkelmand3.setDatum(new GregorianCalendar());
            winkelmand3.setBesteld(true);

            User u3 = userRepository.findById(Long.valueOf(2));
            winkelmand.setUser(userRepository.save(u3));
            winkelmandRepository.save(winkelmand3);
        //}
    }
}
