package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.User;
import com.realdolmen.thomasmore.domain.UserRol;
import com.realdolmen.thomasmore.repository.UserRepository;
import com.realdolmen.thomasmore.repository.UserRolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserRolRepository userRolRepository;

    @Autowired
    private UserRolRepository userRolRepository;

    public void createUser(UserRol rol, String voornaam, String achternaam, String email, String straatEnHuisnummer, int postcode, String gemeente, String password) {
        User user = new User();
        user.setRol(rol);
        user.setVoornaam(voornaam);
        user.setAchternaam(achternaam);
        user.setEmail(email);
        user.setStraatEnHuisnummer(straatEnHuisnummer);
        user.setPostcode(postcode);
        user.setGemeente(gemeente);
        user.setPassword(password);

        userRepository.save(user);
    }

    public void createDummyData() {
        User user1 = new User();
        UserRol userRol1 = userRolRepository.findById(1);

        user1.setRol(userRol1);
        user1.setVoornaam("Bob");
        user1.setAchternaam("Opstalle");
        user1.setEmail("bob@gmail.com");
        user1.setStraatEnHuisnummer("De straat 25");
        user1.setPostcode(2300);
        user1.setGemeente("Geel");
        user1.setPassword("bob");
        userRepository.save(user1);

        User user2 = new User();
        UserRol userRol2 = userRolRepository.findById(1);

        user2.setRol(userRol1);
        user2.setVoornaam("Frederik");
        user2.setAchternaam("Verstappen");
        user2.setEmail("frederik@gmail.com");
        user2.setStraatEnHuisnummer("Lindelaan 16");
        user2.setPostcode(2460);
        user2.setGemeente("Kasterlee");
        user2.setPassword("frederik");
        userRepository.save(user2);
    }

    public void updateUser(User user) {
        userRepository.save(user);
    }

    public void createDummyData() {
        User user = new User();
        user.setRol(userRolRepository.findByFunctie("user"));
        user.setVoornaam("Ellen");
        user.setAchternaam("Peeters");
        user.setEmail("user@test.com");
        user.setStraatEnHuisnummer("Ijsbergstraat 4");
        user.setPostcode(2500);
        user.setGemeente("Lier");
        user.setPassword("iets");
        userRepository.save(user);

        User admin = new User();
        admin.setRol(userRolRepository.findByFunctie("admin"));
        admin.setVoornaam("Axel");
        admin.setAchternaam("Pauwels");
        admin.setEmail("admin@test.com");
        admin.setStraatEnHuisnummer("Kriekenstraat 15");
        admin.setPostcode(2500);
        admin.setGemeente("Lier");
        admin.setPassword("test");
        userRepository.save(admin);
    }

    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    public User findById(Long id) { return userRepository.findById(id); }

    public List findAllByAchternaamStartsWithZoekstring(String zoekstring) { return userRepository.findAllByAchternaamStartsWith(zoekstring); }

    public List<User> findAllByPostcode(int postcode) { return userRepository.findAllByPostcode(postcode); }

    public List<User> findAllByGemeente(String gemeente) { return userRepository.findAllByGemeente(gemeente); }

    public User findByEmail(String email) { return userRepository.findByEmail(email); }

    public User authenticateUser(String email, String password){
        User user = userRepository.findByEmail(email);
        if (user != null){
            if(user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

    public boolean controleerGegevens(String email, String password) {
        boolean check;
        User user = userRepository.findByEmail(email);

        if(user.getPassword() == password) {
            check = true;
        } else {
            check = false;
        }

        return check;
    }

    public void save(User user) { userRepository.save(user); }
}
