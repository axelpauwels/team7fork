package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.Betaalwijze;
import com.realdolmen.thomasmore.repository.BetaalwijzeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BetaalwijzeService {

    @Autowired
    private BetaalwijzeRepository betaalwijzeRepository;

    public void createBetaalwijze(String naam) {
        Betaalwijze betaalwijze = new Betaalwijze();
        betaalwijze.setNaam(naam);

        betaalwijzeRepository.save(betaalwijze);
    }

    public List<Betaalwijze> getAllBetaalwijzen(){
        return betaalwijzeRepository.findAll();
    }

    public void save(Betaalwijze betaalwijze) {
        betaalwijzeRepository.save(betaalwijze);
    }
}
