package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.SupportTicket;
import com.realdolmen.thomasmore.domain.User;
import com.realdolmen.thomasmore.repository.SupportTicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.GregorianCalendar;
import java.util.List;

@Service
public class SupportTicketService {

    @Autowired
    private SupportTicketRepository supportTicketRepository;

    public void createSupportTicket(User user, String onderwerp, String inhoud, GregorianCalendar datum) {
        SupportTicket ticket = new SupportTicket();
        ticket.setUser(user);
        ticket.setOnderwerp(onderwerp);
        ticket.setInhoud(inhoud);
        ticket.setDatum(datum);
        ticket.setIsAfgehandeld(false);

        supportTicketRepository.save(ticket);
    }

    public void updateSupportTicket(SupportTicket ticket) {
        supportTicketRepository.save(ticket);
    }

    public List<SupportTicket> findAllTickets() {
        return supportTicketRepository.findAllByOrderByDatumDesc();
    }

    public List<SupportTicket> findAllByUser(User user) { return supportTicketRepository.findAllByUserOrderByDatumDesc(user); }

    public List<SupportTicket> findAllByUserAndIsAfgehandeld(User user, boolean isAfgehandeld) { return supportTicketRepository.findAllByUserAndIsAfgehandeld(user, isAfgehandeld); }

    public List<SupportTicket> findAllByUserAndDatum(User user, GregorianCalendar datum) { return supportTicketRepository.findAllByUserAndDatum(user, datum); }

    public List<SupportTicket> findAllByUserAndOnderwerp(User user, String onderwerp) { return supportTicketRepository.findAllByUserAndOnderwerp(user, onderwerp); }

    public SupportTicket findById(Long id) { return supportTicketRepository.findById(id); }

    public void save(SupportTicket ticket) {
        supportTicketRepository.save(ticket);
    }
}
