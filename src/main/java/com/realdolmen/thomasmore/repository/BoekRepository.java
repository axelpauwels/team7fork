package com.realdolmen.thomasmore.repository;

import com.realdolmen.thomasmore.domain.Auteur;
import com.realdolmen.thomasmore.domain.Boek;
import com.realdolmen.thomasmore.domain.Genre;
import com.realdolmen.thomasmore.domain.Uitgeverij;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BoekRepository extends CrudRepository<Boek, Long> {
    Boek findBoekById(long id);
    List<Boek> findAll();
    List<Boek> findByPrijs(double prijs);
    List<Boek> findByAuteur(Auteur auteur);
    List<Boek> findByGenre(Genre genre);
    List<Boek> findByUitgeverij(Uitgeverij uitgeverij);
    List<Boek> findByVoorraadBetween(int voorraadMin, int voorraadMax);

}
