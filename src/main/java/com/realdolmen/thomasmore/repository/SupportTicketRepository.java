package com.realdolmen.thomasmore.repository;

import com.realdolmen.thomasmore.domain.SupportTicket;
import com.realdolmen.thomasmore.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.GregorianCalendar;
import java.util.List;

/**
 * Een interface die CrudRepository extend, we geven het type van de entity mee en de primary key. (Employee en Long)
 */
@Repository
public interface SupportTicketRepository extends CrudRepository<SupportTicket, Long> {

    /**
     * Beschrijf in de method name wat je wilt dat spring uit de database haalt, zo simpel is het!
     */
    List<SupportTicket> findAllByUserOrderByDatumDesc(User user);
    SupportTicket findById(Long id);
    List<SupportTicket> findAllByUserAndOnderwerp(User user, String onderwerp);
    List<SupportTicket> findAllByUserAndIsAfgehandeld(User user, boolean isAfgehandeld);
    List<SupportTicket> findAllByUserAndDatum(User user, GregorianCalendar datum);
    List<SupportTicket> findAllByOrderByDatumDesc();
}
