package com.realdolmen.thomasmore.repository;

import com.realdolmen.thomasmore.domain.UserRol;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Een interface die CrudRepository extend, we geven het type van de entity mee en de primary key. (Employee en Long)
 */
@Repository
public interface UserRolRepository extends CrudRepository<UserRol, Long> {

    /**
     * Beschrijf in de method name wat je wilt dat spring uit de database haalt, zo simpel is het!
     */
    UserRol findById (long id);
    UserRol findByLevel(int level);
    UserRol findByFunctie(String functie);
    List<UserRol> findAll();
}
