package com.realdolmen.thomasmore.repository;

import com.realdolmen.thomasmore.domain.Genre;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GenreRepository extends CrudRepository<Genre, Long> {

    Genre findByNaam(String naam);
    Genre findGenreById(long id);
    List<Genre> findAll();

}
