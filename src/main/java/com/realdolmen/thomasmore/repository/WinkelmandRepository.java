package com.realdolmen.thomasmore.repository;

import com.realdolmen.thomasmore.domain.User;
import com.realdolmen.thomasmore.domain.Winkelmand;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.GregorianCalendar;
import java.util.List;

@Repository
public interface WinkelmandRepository extends CrudRepository<Winkelmand,Long> {
    List<Winkelmand> findAll();
    List<Winkelmand> findAllByUser(User user);
    List<Winkelmand> findAllByDatumBetween(GregorianCalendar datum, GregorianCalendar datum2);
    List<Winkelmand> findAllByUserAndIsBesteld(User user, boolean isBesteld);
    List<Winkelmand> findAllByIsBesteld(Boolean isBestald);
    Winkelmand findById(long id);
    void deleteAllByUser(User user);
}
