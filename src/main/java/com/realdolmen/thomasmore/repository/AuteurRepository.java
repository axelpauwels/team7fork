package com.realdolmen.thomasmore.repository;

import com.realdolmen.thomasmore.domain.Auteur;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuteurRepository extends CrudRepository<Auteur, Long> {
    Auteur findByNaam(String naam);
    Auteur findAuteurById(long id);
    List<Auteur> findAll();
}
