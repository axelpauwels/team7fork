package com.realdolmen.thomasmore.repository;

import com.realdolmen.thomasmore.domain.Betaalwijze;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BetaalwijzeRepository extends CrudRepository<Betaalwijze,Long> {
    List<Betaalwijze> findAll();
}
