package com.realdolmen.thomasmore.repository;

import com.realdolmen.thomasmore.domain.Uitgeverij;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UitgeverijRepository extends CrudRepository<Uitgeverij, Long> {

    List<Uitgeverij> findAll();
    Uitgeverij findUitgeverijById(long id);

}
