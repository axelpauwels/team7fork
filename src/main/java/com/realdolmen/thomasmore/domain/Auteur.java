package com.realdolmen.thomasmore.domain;

import com.realdolmen.thomasmore.domain.Boek;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Auteur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany(mappedBy = "auteur")
    private List<Boek> boeken = new ArrayList<Boek>();
    private String naam;

    public Auteur(){

    }

    public Auteur(String naam) {
        this.naam = naam;
    }

    public Auteur(long id, String naam){
        this.id = id;
        this.naam = naam;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Boek> getBoeken() {
        return boeken;
    }

    public void setBoeken(List<Boek> boeken) {
        this.boeken = boeken;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    @Override
    public String toString() {
        return this.naam;
    }
}
