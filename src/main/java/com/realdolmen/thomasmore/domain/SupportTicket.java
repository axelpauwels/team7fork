package com.realdolmen.thomasmore.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

@Entity
public class SupportTicket implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private User user;

    private String onderwerp;
    private String inhoud;
    private GregorianCalendar datum;
    private boolean isAfgehandeld;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getOnderwerp() {
        return onderwerp;
    }

    public void setOnderwerp(String onderwerp) {
        this.onderwerp = onderwerp;
    }

    public String getInhoud() {
        return inhoud;
    }

    public void setInhoud(String inhoud) {
        this.inhoud = inhoud;
    }

    public GregorianCalendar getDatum() {
        return datum;
    }

    public void setDatum(GregorianCalendar datum) {
        this.datum = datum;
    }

    public boolean getIsAfgehandeld() {
        return isAfgehandeld;
    }

    public void setIsAfgehandeld(boolean isAfgehandeld) {
        this.isAfgehandeld = isAfgehandeld;
    }

    public String toonAfgehandeld() {
        String text = "";

        if (isAfgehandeld) {
            text = "Ja";
        } else {
            text = "Nee";
        }

        return text;
    }

    public String convertDatum() {
        SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
        fmt.setCalendar(datum);
        String dateFormatted = fmt.format(datum.getTime());
        return dateFormatted;
    }
}
