package com.realdolmen.thomasmore.domain;

import sun.security.krb5.internal.Ticket;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private UserRol rol;
    private String voornaam;
    private String achternaam;
    private String straatEnHuisnummer;
    private int postcode;
    private String gemeente;
    private String email;
    private String password;
    @OneToMany(mappedBy = "user")
    private List<SupportTicket> tickets = new ArrayList<SupportTicket>();

    @OneToMany(mappedBy = "user")
    private List<Winkelmand> winkelmanden = new ArrayList<Winkelmand>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserRol getRol() {
        return rol;
    }

    public void setRol(UserRol rol) {
        this.rol = rol;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public String getStraatEnHuisnummer() {
        return straatEnHuisnummer;
    }

    public void setStraatEnHuisnummer(String straatEnHuisnummer) {
        this.straatEnHuisnummer = straatEnHuisnummer;
    }

    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    public String getGemeente() {
        return gemeente;
    }

    public void setGemeente(String gemeente) {
        this.gemeente = gemeente;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Winkelmand> getWinkelmanden() {
        return winkelmanden;
    }

    public void setWinkelmanden(List<Winkelmand> winkelmanden) {
        this.winkelmanden = winkelmanden;
    }

    public List<SupportTicket> getTickets() {
        return tickets;
    }

    public void setTickets(List<SupportTicket> tickets) {
        this.tickets = tickets;
    }
}