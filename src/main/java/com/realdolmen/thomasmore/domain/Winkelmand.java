package com.realdolmen.thomasmore.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

@Entity
public class Winkelmand implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private User user;
    private GregorianCalendar datum;
    @ManyToOne
    private Betaalwijze betaalwijze;
    private Boolean isBesteld;
    @OneToMany(mappedBy = "winkelmand", cascade = {CascadeType.REMOVE}, fetch = FetchType.EAGER) //fetch.lazy geeft error
    private List<Item> items = new ArrayList<Item>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public GregorianCalendar getDatum() {
        return datum;
    }

    public void setDatum(GregorianCalendar datum) {
        this.datum = datum;
    }

    public Betaalwijze getBetaalwijze() {
        return betaalwijze;
    }

    public void setBetaalwijze(Betaalwijze betaalwijze) {
        this.betaalwijze = betaalwijze;
    }

    public Boolean getBesteld() {
        return isBesteld;
    }

    public void setBesteld(Boolean besteld) {
        isBesteld = besteld;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public double getTotalePrijs() {
        double prijs = 0;
        for(Item i : getItems()) {
            prijs += (i.getBoek().getPrijs() * (double) i.getAantal());
        }
        return prijs;
    }
}
