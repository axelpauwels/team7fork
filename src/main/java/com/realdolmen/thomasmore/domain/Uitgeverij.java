package com.realdolmen.thomasmore.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Uitgeverij implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String naam;
    private String beschrijving;
    @OneToMany(mappedBy = "uitgeverij")
    private List<Boek> boeken = new ArrayList<Boek>();

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getNaam() { return naam; }

    public void setNaam(String naam) { this.naam = naam; }

    public String getBeschrijving() { return beschrijving; }

    public void setBeschrijving(String beschrijving) { this.beschrijving = beschrijving; }

    public List<Boek> getBoeken() { return boeken; }

    public void setBoeken(List<Boek> boeken) { this.boeken = boeken; }

    @Override
    public String toString() {
        return this.naam;
    }
}
