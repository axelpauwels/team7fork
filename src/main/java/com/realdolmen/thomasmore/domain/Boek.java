package com.realdolmen.thomasmore.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Boek implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Genre genre;
    @ManyToOne
    private Auteur auteur;
    @ManyToOne
    private Uitgeverij uitgeverij;
    @OneToMany(mappedBy = "boek")
    private List<Item> items = new ArrayList<Item>();

    private String titel;
    private String beschrijving;
    private double prijs;
    private int voorraad;

    /** GETTERS & SETTERS */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Auteur getAuteur() {
        return auteur;
    }

    public void setAuteur(Auteur auteur) {
        this.auteur = auteur;
    }

    public Uitgeverij getUitgeverij() {
        return uitgeverij;
    }

    public void setUitgeverij(Uitgeverij uitgeverij) {
        this.uitgeverij = uitgeverij;
    }

    public List<Item> getItems() { return items; }

    public void setItems(List<Item> items) { this.items = items; }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getBeschrijving() {
        return beschrijving;
    }

    public void setBeschrijving(String beschrijving) {
        this.beschrijving = beschrijving;
    }

    public double getPrijs() {
        return prijs;
    }

    public void setPrijs(double prijs) { this.prijs = prijs; }

    public int getVoorraad() {
        return voorraad;
    }

    public void setVoorraad(int voorraad) {
        this.voorraad = voorraad;
    }

}
