package com.realdolmen.thomasmore.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Item implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Boek boek;
    private int aantal;
    @ManyToOne
    private Winkelmand winkelmand;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Boek getBoek() { return boek; }

    public void setBoek(Boek boek) { this.boek = boek; }

    public int getAantal() { return aantal; }

    public void setAantal(int aantal) { this.aantal = aantal; }

    public Winkelmand getWinkelmand() {
        return winkelmand;
    }

    public void setWinkelmand(Winkelmand winkelmand) {
        this.winkelmand = winkelmand;
    }
}
