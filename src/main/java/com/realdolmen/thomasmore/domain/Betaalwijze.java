package com.realdolmen.thomasmore.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Betaalwijze implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String naam;
    @OneToMany(mappedBy = "betaalwijze", cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY)
    private List<Winkelmand> winkelmanden = new ArrayList<Winkelmand>();

    public Betaalwijze() {}

    public Betaalwijze(String naam) {
        this.naam = naam;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public List<Winkelmand> getWinkelmanden() {
        return winkelmanden;
    }

    public void setWinkelmanden(List<Winkelmand> winkelmanden) {
        this.winkelmanden = winkelmanden;
    }
}
